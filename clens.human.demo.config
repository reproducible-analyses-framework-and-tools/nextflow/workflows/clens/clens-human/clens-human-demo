params.dummy_file = "${params.ref_dir}/dummy_file"

params.immuno$parse_immuno_manifest$molecule_filter = ''
params.immuno$parse_immuno_manifest$separator = '\t'

# DNA Alignment
params.clens$alignment$manifest_to_sr_dna_alns$fq_trim_tool = "fastp"
params.clens$alignment$manifest_to_sr_dna_alns$fq_trim_tool_parameters = "[]"
params.clens$alignment$manifest_to_sr_dna_alns$aln_tool = "bwa-mem2"
params.clens$alignment$manifest_to_sr_dna_alns$aln_tool_parameters = "[]"
params.clens$alignment$manifest_to_sr_dna_alns$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.fa"
params.clens$alignment$manifest_to_sr_dna_alns$gtf = "${params.ref_dir}/gencode.v36.annotation.gtf"

# DNA Alignment Processing
params.clens$alignment$alns_to_dna_procd_alns$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.fa"
params.clens$alignment$alns_to_dna_procd_alns$bed = "${params.ref_dir}/hg38.gencode_v36.exome.bed"
params.clens$alignment$alns_to_dna_procd_alns$gtf = ""
params.clens$alignment$alns_to_dna_procd_alns$dup_marker_tool = "picard2"
params.clens$alignment$alns_to_dna_procd_alns$dup_marker_tool_parameters = "[]"
params.clens$alignment$alns_to_dna_procd_alns$base_recalibrator_tool = "gatk4"
params.clens$alignment$alns_to_dna_procd_alns$base_recalibrator_tool_parameters = "[]"
params.clens$alignment$alns_to_dna_procd_alns$indel_realign_tool = "abra2"
params.clens$alignment$alns_to_dna_procd_alns$indel_realign_tool_parameters = "[]"
params.clens$alignment$alns_to_dna_procd_alns$known_sites_ref = "${params.ref_dir}/Homo_sapiens_assembly38.dbsnp138.vcf.gz"

# RNA Alignment
params.clens$alignment$manifest_to_sr_rna_alns$fq_trim_tool = "fastp"
params.clens$alignment$manifest_to_sr_rna_alns$fq_trim_tool_parameters = "[]"
params.clens$alignment$manifest_to_sr_rna_alns$aln_tool = "star"
params.clens$alignment$manifest_to_sr_rna_alns$aln_tool_parameters = "['star': '--quantMode TranscriptomeSAM --outSAMtype BAM SortedByCoordinate --twopassMode Basic --outSAMunmapped Within']"
params.clens$alignment$manifest_to_sr_rna_alns$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.fa"
params.clens$alignment$manifest_to_sr_rna_alns$gtf = "${params.ref_dir}/gencode.v36.annotation.gtf"

# RNA Alignment Processing
params.clens$alignment$alns_to_rna_procd_alns$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.fa"
params.clens$alignment$alns_to_rna_procd_alns$bed = "${params.ref_dir}/hg38.gencode_v36.exome.bed"
params.clens$alignment$alns_to_rna_procd_alns$gtf = "${params.ref_dir}/gencode.v36.annotation.gtf"
params.clens$alignment$alns_to_rna_procd_alns$dup_marker_tool = ""
params.clens$alignment$alns_to_rna_procd_alns$dup_marker_tool_parameters = "[]"
params.clens$alignment$alns_to_rna_procd_alns$base_recalibrator_tool = ""
params.clens$alignment$alns_to_rna_procd_alns$base_recalibrator_tool_parameters = "[]"
params.clens$alignment$alns_to_rna_procd_alns$indel_realign_tool = "abra2_rna"
params.clens$alignment$alns_to_rna_procd_alns$indel_realign_tool_parameters = "['abra2_rna':'--dist 500000 --sua']"
params.clens$alignment$alns_to_rna_procd_alns$known_sites_ref = "${params.ref_dir}/dummy_file"

# Transcript Quantification from RNA Alignments
params.clens$rna_quant$alns_to_transcript_counts$rna_ref = "${params.ref_dir}/Homo_sapiens.assembly38.fa"
params.clens$rna_quant$alns_to_transcript_counts$gtf = "${params.ref_dir}/gencode.v36.annotation.gtf"
params.clens$rna_quant$alns_to_transcript_counts$tx_quant_tool = "salmon"
params.clens$rna_quant$alns_to_transcript_counts$tx_quant_tool_parameters = "[]"

# Somatic Variant Calling
params.clens$somatic$alns_to_som_vars$som_var_caller = "strelka2,mutect2,varscan2,abra2"
params.clens$somatic$alns_to_som_vars$som_var_caller_parameters = "['gatk_filter_mutect_calls_suffix': '.gfilt', 'varscan2': '--output-vcf']"
params.clens$somatic$alns_to_som_vars$som_var_caller_suffix = "['mutect2': '.mutect2', 'strelka2': '.strelka2', 'abra2': '.abra2', 'varscan2': '.varscan2']"
params.clens$somatic$alns_to_som_vars$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.fa"
params.clens$somatic$alns_to_som_vars$bed = "${params.ref_dir}/hg38.gencode_v36.exome.bed"
params.clens$somatic$alns_to_som_vars$som_var_pon_vcf = "${params.ref_dir}/1000g_pon.hg38.vcf.gz"
params.clens$somatic$alns_to_som_vars$som_var_af_vcf = "${params.ref_dir}/af-only-gnomad.hg38.vcf.gz"
params.clens$somatic$alns_to_som_vars$known_sites_ref = "${params.ref_dir}/small_exac_common_3.hg38.vcf.gz"
params.clens$somatic$alns_to_som_vars$species = "homo sapiens"

# Somatic Variant Filtering
params.clens$somatic$som_vars_to_filtd_som_vars$vcf_filtering_tool = 'bcftools'
params.clens$somatic$som_vars_to_filtd_som_vars$vcf_filtering_tool_parameters = "[]"

# SNV/InDel Filtering
params.clens$snpsift_filter_snvs$snpsift_snv_filter_parameters = "ANN[*].EFFECT has 'missense_variant'"
params.clens$snpsift_filter_indels$snpsift_indel_filter_parameters = "(ANN[*].EFFECT has 'conservative_inframe_insertion') || (ANN[*].EFFECT has 'conservative_inframe_deletion') || (ANN[*].EFFECT has 'disruptive_inframe_insertion') || (ANN[*].EFFECT has 'disruptive_inframe_deletion') || (ANN[*].EFFECT has 'frameshift_variant')"

# Somatic InDel Normalization
params.clens$somatic$som_vars_to_normd_som_vars$vcf_norming_tool = 'bcftools'
params.clens$somatic$som_vars_to_normd_som_vars$vcf_norming_tool_parameters = "[]"
params.clens$somatic$som_vars_to_normd_som_vars$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.fa"

# Somatic Variant Merging
params.clens$somatic$combine_strategy = "union"
# Intersection
params.clens$somatic$som_vars_to_isecd_som_vars$vcf_isecing_tool = "bedtools"
params.clens$somatic$som_vars_to_isecd_som_vars$vcf_isecing_tool_parameters = "[]"
# Union
params.clens$somatic$som_vars_to_union_som_vars$vcf_merging_tool = 'jacquard_merge'
params.clens$somatic$som_vars_to_union_som_vars$vcf_merging_tool_parameters = "['jacquard_merge': '--include_format_tags=\"GT,AF,AU,CU,GU,TU,TAR,TIR,FREQ,VAF\"']"

# Variant Annotation
params.clens$snpeff$annot_tool_ref = "${params.ref_dir}/GRCh38.GENCODEv36"

# Germline Variant Calling
params.clens$germline$sr_alns_to_germ_vars$germ_var_caller = 'deepvariant'
params.clens$germline$sr_alns_to_germ_vars$germ_var_caller_parameters = "['deepvariant': '--model_type WES']"
params.clens$germline$sr_alns_to_germ_vars$germ_var_caller_suffix = "['deepvariant': '.deepv']"
params.clens$germline$sr_alns_to_germ_vars$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.fa"
params.clens$germline$sr_alns_to_germ_vars$bed = "${params.ref_dir}/hg38.gencode_v36.exome.bed"

# Germline Variant Filtering
params.clens$germline$germ_vars_to_filtd_germ_vars$vcf_filtering_tool = 'bcftools'
params.clens$germline$germ_vars_to_filtd_germ_vars$vcf_filtering_tool_parameters = "[]"

# Misc Variant
params.clens$bcftools$bcftools_index$bcftools_index_parameters = ''
params.clens$bcftools$bcftools_index_somatic$bcftools_index_somatic_parameters = ''

# Somatic + Germline Merging
params.clens$seq_variation$germ_and_som_vars_to_tumor_vars$vcf_merge_tool = 'jacquard_merge'
params.clens$seq_variation$germ_and_som_vars_to_tumor_vars$vcf_merge_tool_parameters = "[]"

# Phasing of Somatic + Germline Variants
params.clens$seq_variation$make_phased_tumor_vars$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.fa"
params.clens$seq_variation$make_phased_tumor_vars$gtf = "${params.ref_dir}/gencode.v36.annotation.gtf"
params.clens$seq_variation$make_phased_tumor_vars$species = 'human'
params.clens$seq_variation$make_phased_tumor_vars$var_phaser_tool = 'whatshap'
params.clens$seq_variation$make_phased_tumor_vars$var_phaser_tool_parameters = "['whatshap': '--ignore-read-groups']"

# Phasing of Germline Variants
params.clens$seq_variation$make_phased_germline_vars$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.fa"
params.clens$seq_variation$make_phased_germline_vars$gtf = "${params.ref_dir}/gencode.v36.annotation.gtf"
params.clens$seq_variation$make_phased_germline_vars$var_phaser_tool = 'whatshap'
params.clens$seq_variation$make_phased_germline_vars$var_phaser_tool_parameters = "['whatshap': '--ignore-read-groups']"

# MHC Calling
params.clens$immuno$procd_fqs_to_mhc_alleles$aln_tool = ""
params.clens$immuno$procd_fqs_to_mhc_alleles$aln_tool_parameters = '--outSAMtype BAM SortedByCoordinate'
params.clens$immuno$procd_fqs_to_mhc_alleles$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.fa"
params.clens$immuno$procd_fqs_to_mhc_alleles$mhc_caller_tool = 'seq2hla'
params.clens$immuno$procd_fqs_to_mhc_alleles$mhc_caller_tool_parameters = "[]"

# Tumor Purity
params.clens$onco$alns_to_tumor_purities$tx_quant_tool = ''
params.clens$onco$alns_to_tumor_purities$tx_quant_tool_parameters = "[]"
params.clens$onco$alns_to_tumor_purities$tumor_purities_tool = 'sequenza'
params.clens$onco$alns_to_tumor_purities$tumor_purities_tool_parameters = "[sequenza_gc_wiggle: '-w 50', sequenza_bam2seqz: '', sequenza_seqz_binning: '-w 50']"
params.clens$onco$alns_to_tumor_purities$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.fa"
params.clens$onco$alns_to_tumor_purities$gtf = "${params.ref_dir}/gencode.v36.annotation.gtf"
params.clens$onco$alns_to_tumor_purities$bed = "${params.ref_dir}/hg38.gencode_v36.exome.bed"

# CNA and cancer cell fraction
params.clens$onco$alns_to_cnas$cna_tool = 'cnvkit'
params.clens$onco$alns_to_cnas$cna_tool_refs = ''
params.clens$onco$alns_to_cnas$cna_tool_parameters = "[cnvkit_segment: '--drop-low-coverage', cnvkit_call: '--ploidy 2']"
params.clens$onco$alns_to_cnas$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.fa"
params.clens$onco$alns_to_cnas$bed = "${params.ref_dir}/hg38.gencode_v36.exome.bed"
params.clens$onco$alns_to_cnas$gtf = "${params.ref_dir}/gencode.v36.annotation.gtf"

# SNVs Neoantigen Workflow
params.clens$neos$snvs_to_neos$gtf = "${params.ref_dir}/gencode.v36.annotation.gtf"
params.clens$neos$snvs_to_neos$dna_ref = "${params.ref_dir}/Homo_sapiens.assembly38.fa"
params.clens$neos$snvs_to_neos$pep_ref = "${params.ref_dir}/gencode.v36.pc_translations.fa"
params.clens$neos$snvs_to_neos$som_var_type = "SNV"
params.clens$neos$snvs_to_neos$lenstools_filter_expressed_variants_parameters = "-p 75"
params.clens$neos$snvs_to_neos$bcftools_index_phased_germline_parameters = ""
params.clens$neos$snvs_to_neos$bcftools_index_phased_tumor_parameters = ""
params.clens$neos$snvs_to_neos$lenstools_get_expressed_transcripts_bed_parameters = ""
params.clens$neos$snvs_to_neos$samtools_faidx_fetch_somatic_folder_parameters = ""

# InDels Neoantigen Workflow
params.clens$neos$indels_to_neos$gtf = "${params.ref_dir}/gencode.v36.annotation.gtf"
params.clens$neos$indels_to_neos$dna_ref = "${params.ref_dir}/Homo_sapiens.assembly38.fa"
params.clens$neos$indels_to_neos$pep_ref = "${params.ref_dir}/gencode.v36.pc_translations.fa"
params.clens$neos$indels_to_neos$som_var_type = "InDel"
params.clens$neos$indels_to_neos$lenstools_filter_expressed_variants_parameters = "-p 75"
params.clens$neos$indels_to_neos$bcftools_index_phased_germline_parameters = ""
params.clens$neos$indels_to_neos$bcftools_index_phased_tumor_parameters = ""
params.clens$neos$indels_to_neos$lenstools_get_expressed_transcripts_bed_parameters = ""
params.clens$neos$indels_to_neos$samtools_faidx_fetch_somatic_folder_parameters = ""

# pMHC Summarization
params.clens$immuno$peps_and_alleles_to_antigen_stats$antigen_tool = 'netmhcpan'
params.clens$immuno$peps_and_alleles_to_antigen_stats$antigen_tool_parameters = '[]'
params.clens$immuno$peps_and_alleles_to_antigen_stats$antigen_tool_ref_dir = "['antigen_garnish': \"${params.ref_dir}/antigen.garnish\"]"
params.clens$immuno$peps_and_alleles_to_antigen_stats$species = 'human'
params.clens$immuno$peps_and_alleles_to_antigen_stats$peptide_lengths = '8,9,10,11'

# Agretopicity Calculation
params.clens$immuno$calculate_agretopicity$blastp_db_dir = "${params.ref_dir}/antigen.garnish"
params.clens$immuno$calculate_agretopicity$peps_and_alleles_to_antigen_stats$antigen_tool = 'netmhcpan'
params.clens$immuno$calculate_agretopicity$peps_and_alleles_to_antigen_stats$antigen_tool_parameters = '[]'
params.clens$immuno$calculate_agretopicity$peps_and_alleles_to_antigen_stats$antigen_tool_ref_dir = "['antigen_garnish': \"${params.ref_dir}/antigen.garnish\"]"
params.clens$immuno$calculate_agretopicity$peps_and_alleles_to_antigen_stats$species = 'human'
params.clens$immuno$calculate_agretopicity$peps_and_alleles_to_antigen_stats$peptide_lengths = '8,9,10,11'

# Peptide Quantification
params.clens$lenstools$lenstools_get_snv_peptide_read_count$gtf = "${params.ref_dir}/gencode.v36.annotation.gtf"
params.clens$lenstools$lenstools_get_indel_peptide_read_count$gtf = "${params.ref_dir}/gencode.v36.annotation.gtf"

# Sample Swap Checks
params.clens$qc$bams_to_sample_swap_check$sample_swap_tool = 'somalier'
params.clens$qc$bams_to_sample_swap_check$sample_swap_tool_dna_ref = "${params.ref_dir}/Homo_sapiens.assembly38.fa"
params.clens$qc$bams_to_sample_swap_check$sample_swap_tool_known_sites = "${params.ref_dir}/somalier.sites.hg38.vcf.gz"
params.clens$qc$bams_to_sample_swap_check$sample_swap_tool_parameters = "[]"

# Non-CTA Self Peptide Filtering
params.clens$lenstools$lenstools_filter_mutant_peptides$pep_ref = "${params.ref_dir}/gencode.v36.pc_translations.fa"

# pMHC Filtering
params.clens$lenstools$lenstools_annotate_pmhcs$binding_affinity_threshold = 1000
params.clens$lenstools$lenstools_filter_mutant_peptides = "${params.ref_dir}/gencode.v36.pc_translations.fa"

# QC
params.clens$qc$samtools_get_targeted_cov_stats$bed = "${params.ref_dir}/hg38.gencode_v36.exome.bed"

# FDA Reporting
params.clens$lenstools$lenstools_get_fda_stats$parameters = "--submission-number abc123 --norm-dna-depth 150 --tumor-dna-depth 300"

# LENS output directory
params.lens_out_dir = "${params.output_dir}/clens"
